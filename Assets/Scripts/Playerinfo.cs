﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerinfo : MonoBehaviour {
    private static Playerinfo playerinfo;
    private string playerName;

    private Playerinfo()
    {

    }

    public static Playerinfo GetInstance()
    {
        if (playerinfo == null)
            playerinfo = new Playerinfo();

        return playerinfo;
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPlayerName(string val)
    {
        playerinfo.playerName = val;
    }

    public string GetPlayerName()
    {
        return playerName;
    }
}
