﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class InputFieldController : MonoBehaviour {

    public InputField field;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SubmitInputFieldValue()
    {
        if (field.text != "")
        {
            Playerinfo.GetInstance().SetPlayerName(field.text);
            SceneManager.LoadScene("v2");
        }
    }
}
